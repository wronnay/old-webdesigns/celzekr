# celzekr

![](screenshot-2014-01-29.png)

**License:** GCC BY-NC-SA 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0/)

## Background

celzekr was a Website where I offered many free Webdesign Templates. (You probably see many of them at the same place where you watch this File)

I used my own CMS and PHP Files for the service but lost most of the custom code.